//всплывающие окна (выбора настроек)

$(document).ready(function () {

});

var fadespeed = 150;


function windowsPanels_show(){
    $('#windowsPanels').fadeIn(fadeSpeed)
}
function windowsPanels_hide() {
    $('#windowsPanels').fadeOut(fadeSpeed)
}

//бинаризация
function binarizationFormPanel_show() {
    windowsPanels_show();
    contrastFormPanel_hide();
    noise_salt_pepper_formFormPanel_hide();
    $('#binarizationFormPanel').fadeIn(fadeSpeed)
}
function binarizationFormPanel_hide() {
    $('#binarizationFormPanel').fadeOut(fadeSpeed)
}
//контраст
function contrastFormPanel_show() {
    windowsPanels_show();
    binarizationFormPanel_hide();
    noise_salt_pepper_formFormPanel_hide();
    $('#contrastFormPanel').fadeIn(fadeSpeed)
}
function contrastFormPanel_hide() {
    $('#contrastFormPanel').fadeOut(fadeSpeed)
}

//шум соль-перец
function noise_salt_pepper_formFormPanel_show() {
    windowsPanels_show();
    binarizationFormPanel_hide();
    contrastFormPanel_hide()
    $('#noise_salt_pepper_formFormPanel').fadeIn(fadeSpeed)
}
function noise_salt_pepper_formFormPanel_hide() {
    $('#noise_salt_pepper_formFormPanel').fadeOut(fadeSpeed)
}