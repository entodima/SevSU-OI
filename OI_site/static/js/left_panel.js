//постоянные объекты левой панели

$(document).ready(function(){
    $('#imageDeleteFormSubmit').click(function () {
        imageDelete()
    })

    $('#leftPanelHideButton').click(function(){
        $.cookie('leftPanel',false)
        leftPanelHide()
    })
    $('#leftPanelShowButton').click(function(){
        $.cookie('leftPanel',true)
        leftPanelShow()
    })

    $('#leftPanelHideHistory').click(function(){
        historyHide()
    })
    $('#leftPanelShowHistory').click(function(){
        histogramHide()
        historyShow()
    })
    $('#leftPanelHideHistogram').click(function(){
        histogramHide()
    })
    $('#leftPanelShowHistogram').click(function(){
        historyHide()
        histogramShow()
    })

    pageOpen()
})
function imageDelete(){
    if (window.confirm("Удалить изображение?")) { 
        $('#imageDeleteForm').submit()
    }
}

//animations
var fadeSpeed = 150
function pageOpen(){
    $('#leftPanelShowButton').hide()
    $('#leftPanelHideButton').show()
}

function leftPanelShow(){
    $('#leftPanelShowButton').fadeOut(fadeSpeed).queue(function() {
        $('#leftPanelHideButton').fadeIn(fadeSpeed)
        $(this).dequeue()
    })
    $('#editPanel').fadeOut(fadeSpeed).queue(function() {
        $('#leftPanelBack').show();
        $('#editPanel').fadeIn(fadeSpeed)
        $('#leftPanel').fadeIn(fadeSpeed)
        $(this).dequeue()
    })
}
function leftPanelHide(){
    $('#leftPanelHideButton').fadeOut(fadeSpeed).queue(function() {
        $('#leftPanelShowButton').fadeIn(fadeSpeed)
        $(this).dequeue()
    })
    $('#editPanel').fadeOut(fadeSpeed).queue(function() {
        if(!$('#historyPanel').hasClass('hided')){
            $('#historyPanel').hide(); $('#historyPanel').show()
        }
        $('#leftPanelBack').hide();
        $('#editPanel').fadeIn(fadeSpeed)
        $(this).dequeue()
    })
    $('#leftPanel').fadeOut(fadeSpeed)
}

function historyShow(){
    $('#leftPanelShowHistory').fadeOut(fadeSpeed).queue(function(){
        $('#leftPanelHideHistory').fadeIn(fadeSpeed).queue(function () {
            $('#historyPanel').removeClass('hided');
            $(this).dequeue()
        })
        $('#historyPanel').fadeIn(fadeSpeed);
        $(this).dequeue()
    })
}
function historyHide(){
    $('#leftPanelHideHistory').fadeOut(fadeSpeed).queue(function(){
        $('#leftPanelShowHistory').fadeIn(fadeSpeed)
        $('#historyPanel').fadeOut(fadeSpeed).queue(function () {
            $('#historyPanel').addClass('hided');
            $(this).dequeue()
        });
        $(this).dequeue()
    })
}

function histogramShow() {
    $('#leftPanelShowHistogram').fadeOut(fadeSpeed).queue(function(){
        $('#leftPanelHideHistogram').fadeIn(fadeSpeed).queue(function () {
            $('#histogramPanel').removeClass('hided');
            $(this).dequeue()
        })
        $('#histogramPanel').fadeIn(fadeSpeed);
        $(this).dequeue()
    })
}
function histogramHide() {
    $('#leftPanelHideHistogram').fadeOut(fadeSpeed).queue(function(){
        $('#leftPanelShowHistogram').fadeIn(fadeSpeed)
        $('#histogramPanel').fadeOut(fadeSpeed).queue(function () {
            $('#histogramPanel').addClass('hided');
            $(this).dequeue()
        })
        $(this).dequeue()
    })
}