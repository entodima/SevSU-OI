from django.contrib import admin
from .models import Image, ProcessedImage
# Register your models here.

admin.site.register(Image)
admin.site.register(ProcessedImage)