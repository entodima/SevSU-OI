from django.apps import AppConfig


class OiSiteConfig(AppConfig):
    name = 'OI_site'
