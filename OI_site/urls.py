from django.urls import path
from . import views


urlpatterns = [
    path('', views.image_list, name="image_list"),
    path('image/<int:pk>/', views.image_edit, name="image_edit"),
    #Left Panel - file
    path('image/<int:pk>/delete/', views.image_delete, name='image_delete'),
    #Left Panel - edit
    path('image/<int:pk>/undo_back/', views.image_undo_back, name='image_undo_back'),
    path('image/<int:pk>/undo_forward/', views.image_undo_forward, name='image_undo_forward'),

    path('image/<int:pk>/transform_image_to_gray/', views.transform_image_to_gray, name='transform_image_to_gray'),
    path('image/<int:pk>/transform_image_to_bin/', views.transform_image_to_bin, name='transform_image_to_bin'),
    path('image/<int:pk>/transform_image_to_negative/', views.transform_image_to_negative, name='transform_image_to_negative'),
    path('image/<int:pk>/transform_image_set_contrast/', views.transform_image_set_contrast, name='set_contrast'),
    path('image/<int:pk>/noise_salt_pepper/', views.transform_image_set_noise_salt_pepper, name='set_noise_salt_pepper'),

    path('image/<int:pk>/undo_go/<int:history>/', views.image_undo_go, name='image_undo_go'),

    path('image/<int:pk>/transform_image_to_lin/', views.transform_image_to_lin, name='transform_image_to_lin'),
    path('image/<int:pk>/transform_image_to_med/', views.transform_image_to_med, name='transform_image_to_med'),
    path('image/<int:pk>/transform_image_to_kir/', views.transform_image_to_kir, name='transform_image_to_kir'),
    path('image/<int:pk>/transform_image_to_lap/', views.transform_image_to_lap, name='transform_image_to_lap'),
    path('image/<int:pk>/transform_image_to_rob/', views.transform_image_to_rob, name='transform_image_to_rob'),
    path('image/<int:pk>/transform_image_to_sob/', views.transform_image_to_sob, name='transform_image_to_sob'),
    path('image/<int:pk>/transform_image_to_uol/', views.transform_image_to_uol, name='transform_image_to_uol'),
    path('image/<int:pk>/transform_image_to_sta/', views.transform_image_to_sta, name='transform_image_to_sta'),
]
