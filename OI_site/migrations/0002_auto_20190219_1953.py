# Generated by Django 2.1.7 on 2019-02-19 16:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('OI_site', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='processedimage',
            old_name='processed_image',
            new_name='parent_image',
        ),
    ]
