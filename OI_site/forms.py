from django import forms
from .models import Image, ImageToBin, ImageContrast, ImageNoiseSaltPepper


class ImageUploadForm(forms.ModelForm):
    image = forms.ImageField(label='', widget=forms.FileInput(attrs={'onchange': 'submit();'}))

    class Meta:
        model = Image
        fields = ('image',)


class ImageToBinForm(forms.ModelForm):
    bin_high = forms.Field(label='', widget=forms.TextInput(attrs={'class': 'jscolor', 'value': 'FFFFFF', }))
    bin_low = forms.Field(label='', widget=forms.TextInput(attrs={'class': 'jscolor', 'value': '000000', }))
    bin_limit = forms.IntegerField(label='', initial=127)

    class Meta:
        model = ImageToBin
        fields = ('bin_high', 'bin_low', 'bin_limit',)


class ImageContrastForm(forms.ModelForm):
    contrast_value = forms.IntegerField(label='Контраст (±100)', initial=0)
    bright_value = forms.IntegerField(label='Яркость (±100)', initial=0)

    class Meta:
        model = ImageContrast
        fields = ('bright_value', 'contrast_value',)


class ImageNoiseSaltPepperForm(forms.ModelForm):
    value = forms.FloatField(label='0-0.4', widget=forms.TextInput(), min_value=0, max_value=0.4)

    class Meta:
        model = ImageNoiseSaltPepper
        fields = ('value',)
