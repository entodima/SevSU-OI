from shutil import copyfile, move, copy
from scripts.actions_with_image import image_to_gray, image_to_bin, get_image_format, \
    get_image_size, image_to_negative, image_set_contrast, create_histogram, image_set_noise_salt_pepper, image_to_lin,\
    image_to_med, image_to_kir, image_to_lap, image_to_rob, image_to_sob, image_to_uol, image_to_sta

from django.shortcuts import render, get_object_or_404, redirect
from .models import Image, ProcessedImage
from .forms import ImageUploadForm, ImageToBinForm, ImageContrastForm, ImageNoiseSaltPepperForm


def image_list(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ImageUploadForm(request.POST, request.FILES)
            if form.is_valid():
                image = form.save(commit=False)
                image.author = request.user
                image.save()

                processed_path = str(image.image).replace('images/', 'processed_images/')
                copyfile('media/' + str(image.image), 'media/' + processed_path)
                plots = create_histogram(processed_path)
                ProcessedImage.objects.create(
                    image=processed_path,
                    parent_image_id=image.id,
                    about='Исходное',
                    red_histogram=plots[0],
                    green_histogram=plots[1],
                    blue_histogram=plots[2],
                    gray_histogram=plots[3])
                return redirect('image_edit', pk=image.pk)
        else:
            form = ImageUploadForm()
        #  вывод всех объектов без ссылки на другой объект отсортированный по убыванию
        images = Image.objects.filter(author=request.user).order_by('-pk')
        return render(request, 'OI_site/image_list.html', {'images': images, 'form': form})
    return render(request, 'OI_site/login.html', {})


def image_edit(request, pk):
    if request.user.is_authenticated:
        #  image = Image.objects.get(pk=pk)
        image = get_object_or_404(Image, pk=pk)
        if image.author != request.user:
            return redirect('image_list')
        processed_images = ProcessedImage.objects.filter(parent_image=image.pk)
        processed_images_current = processed_images.filter(current=True)
        processed_images_max = processed_images_current.last()

        image_format = get_image_format(image.image)
        image_size = get_image_size(image.image)

        image_to_bin_form = ImageToBinForm()
        image_contrast_form = ImageContrastForm()
        image_noise_salt_pepper_form = ImageNoiseSaltPepperForm()

        return render(request, 'OI_site/image_edit.html', {
            'image': image,
            'processed_images': processed_images.order_by('-pk'),
            'processed_images_max': processed_images_max,
            'processed_images_current': processed_images_current,
            'red_histogram': processed_images_current.last().red_histogram,
            'green_histogram': processed_images_current.last().green_histogram,
            'blue_histogram': processed_images_current.last().blue_histogram,
            'gray_histogram': processed_images_current.last().gray_histogram,
            'image_format': image_format,
            'image_size': image_size,
            'image_to_bin_form': image_to_bin_form,
            'image_contrast_form': image_contrast_form,
            'image_noise_salt_pepper_form': image_noise_salt_pepper_form,
            })
    return redirect('image_list')


def image_delete(request, pk):
    if request.user.is_authenticated:
        image = get_object_or_404(Image, pk=pk)
        if image.author != request.user:
            return redirect('image_list')
        image.delete()
        return redirect('image_list')
    return redirect('image_edit', pk=pk)


def image_undo_back(request, pk):
    if request.user.is_authenticated:
        images = ProcessedImage.objects.filter(parent_image=pk, current=True)
        if images.count() > 1:
            image = images.last()
            image.current = False
            image.save()
        return redirect('image_edit', pk)
    return redirect('image_list')


def image_undo_forward(request, pk):
    if request.user.is_authenticated:
        images = ProcessedImage.objects.filter(parent_image=pk, current=False)
        image = images.first()
        image.current = True
        image.save()
    return redirect('image_edit', pk)


def image_undo_go(request, pk, history):
    if request.user.is_authenticated:
        images = ProcessedImage.objects.filter(parent_image_id=pk, current=True)
        for image in images:
            image.current = False
            image.save()
        images = ProcessedImage.objects.filter(id__lte=int(history), parent_image_id=pk)
        #  id__lte= - там где id <=, gte >=, lt <, gt >
        for image in images:
            image.current = True
            image.save()
    return redirect('image_edit', pk)


def transform_image_to_gray(request, pk):
    if request.user.is_authenticated:
        image = get_object_or_404(Image, pk=pk)
        if image.author != request.user:
            return redirect('image_list')
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk).filter(current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        #  создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "В оттенки серого"

        #  копия файла
        path = str(processed_images_max_new.image)
        copy('media/'+path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0]+'-gray.'+path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/'+path_temp, 'media/'+path_new)

        #  действие с файлом
        plots = image_to_gray(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()

    create_histogram(processed_images_max_new.image)
    return redirect('image_edit', pk)


def transform_image_to_bin(request, pk):
    if request.method == "POST":
        image = get_object_or_404(Image, pk=pk)
        if image.author == request.user:
            form = ImageToBinForm(request.POST)
            if form.is_valid():
                data_for_bin = form.save(commit=False)

                processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk).filter(current=True).last()

                ProcessedImage.objects.filter(current=False).delete()  # удаление старых

                # создание копии объекта
                processed_images_max_new = processed_images_max
                processed_images_max_new.pk = None
                processed_images_max.about = "В бинарное"

                # копия файла
                path = str(processed_images_max_new.image)
                copy('media/' + path, 'media/tmp/')
                path = str(processed_images_max_new.image)
                path_splited = path.split('.')
                path_new = path_splited[0] + '-bin.' + path_splited[-1]
                path_temp = path.replace('processed_images/', 'tmp/')
                move('media/' + path_temp, 'media/' + path_new)

                # действие с файлом
                plots = image_to_bin(path_new, data_for_bin.bin_high, data_for_bin.bin_low, data_for_bin.bin_limit)
                processed_images_max.image = path_new
                processed_images_max_new.red_histogram = plots[0]
                processed_images_max_new.green_histogram = plots[1]
                processed_images_max_new.blue_histogram = plots[2]
                processed_images_max_new.gray_histogram = plots[3]

                processed_images_max_new.save()

        return redirect('image_edit', pk)
    return redirect('image_list')


def transform_image_to_negative(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "В негатив"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-neg.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_negative(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_set_contrast(request, pk):
    if request.method == "POST":
        image = get_object_or_404(Image, pk=pk)
        if image.author == request.user:
            form = ImageContrastForm(request.POST)
            if form.is_valid():
                data_for_cont = form.save(commit=False)

                processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk).filter(current=True).last()

                ProcessedImage.objects.filter(current=False).delete()  # удаление старых

                # создание копии объекта
                processed_images_max_new = processed_images_max
                processed_images_max_new.pk = None
                processed_images_max.about = 'Яркость('+ str(data_for_cont.bright_value) + \
                                             ')/Контраст' + str(data_for_cont.contrast_value)

                # копия файла
                path = str(processed_images_max_new.image)
                copy('media/' + path, 'media/tmp/')
                path = str(processed_images_max_new.image)
                path_splited = path.split('.')
                path_new = path_splited[0] + '-cont.' + path_splited[-1]
                path_temp = path.replace('processed_images/', 'tmp/')
                move('media/' + path_temp, 'media/' + path_new)

                # действие с файлом
                plots = image_set_contrast(path_new, data_for_cont.contrast_value, data_for_cont.bright_value)
                processed_images_max.image = path_new
                processed_images_max_new.red_histogram = plots[0]
                processed_images_max_new.green_histogram = plots[1]
                processed_images_max_new.blue_histogram = plots[2]
                processed_images_max_new.gray_histogram = plots[3]

                processed_images_max_new.save()
        return redirect('image_edit', pk)
    return redirect('image_list')


def transform_image_set_noise_salt_pepper(request, pk):
    if request.method == "POST":
        image = get_object_or_404(Image, pk=pk)
        if image.author == request.user:
            form = ImageNoiseSaltPepperForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)

                processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk).filter(current=True).last()

                ProcessedImage.objects.filter(current=False).delete()  # удаление старых

                # создание копии объекта
                processed_images_max_new = processed_images_max
                processed_images_max_new.pk = None
                processed_images_max.about = "Шум соль/перец: " + str(form.value)

                # копия файла
                path = str(processed_images_max_new.image)
                copy('media/' + path, 'media/tmp/')
                path = str(processed_images_max_new.image)
                path_splited = path.split('.')
                path_new = path_splited[0] + '-bin.' + path_splited[-1]
                path_temp = path.replace('processed_images/', 'tmp/')
                move('media/' + path_temp, 'media/' + path_new)

                # действие с файлом
                plots = image_set_noise_salt_pepper(path_new, form.value)
                processed_images_max.image = path_new
                processed_images_max_new.red_histogram = plots[0]
                processed_images_max_new.green_histogram = plots[1]
                processed_images_max_new.blue_histogram = plots[2]
                processed_images_max_new.gray_histogram = plots[3]

                processed_images_max_new.save()

        return redirect('image_edit', pk)
    return redirect('image_list')

def transform_image_to_lin(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Лин. преобр."

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-lin.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_lin(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_to_med(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Мед. фильтр"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-med.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_med(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_to_kir(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Метод Кирша"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-kir.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_kir(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)

def transform_image_to_lap(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Метод Лапласа"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-lap.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_lap(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_to_rob(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Метод Робертса"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-rob.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_rob(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_to_sob(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Метод Собела"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-sob.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_sob(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)


def transform_image_to_uol(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Метод Уоллеса"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-uol.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_uol(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)

def transform_image_to_sta(request, pk):
    image = get_object_or_404(Image, pk=pk)
    if image.author == request.user:
        processed_images_max = ProcessedImage.objects.filter(parent_image=image.pk, current=True).last()

        ProcessedImage.objects.filter(current=False).delete()  # удаление старых

        # создание копии объекта
        processed_images_max_new = processed_images_max
        processed_images_max_new.pk = None
        processed_images_max.about = "Статистический метод"

        # копия файла
        path = str(processed_images_max_new.image)
        copy('media/' + path, 'media/tmp/')
        path = str(processed_images_max_new.image)
        path_splited = path.split('.')
        path_new = path_splited[0] + '-sta.' + path_splited[-1]
        path_temp = path.replace('processed_images/', 'tmp/')
        move('media/' + path_temp, 'media/' + path_new)

        # действие с файлом
        plots = image_to_sta(path_new)
        processed_images_max.image = path_new
        processed_images_max_new.red_histogram = plots[0]
        processed_images_max_new.green_histogram = plots[1]
        processed_images_max_new.blue_histogram = plots[2]
        processed_images_max_new.gray_histogram = plots[3]

        processed_images_max_new.save()
    return redirect('image_edit', pk)