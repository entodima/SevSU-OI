from django.db import models

# Create your models here.
class Image(models.Model):
    image = models.ImageField(upload_to="images/", blank=True, null=False)
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    def remove(self):
        images = Image.objects.filter(imageSoure=self.pk)
        for image in images:
            image.remove()
        self.delete()

class ProcessedImage(models.Model):
    image = models.ImageField(upload_to="processed_images/", blank=True, null=False)
    parent_image = models.ForeignKey('Image', on_delete=models.CASCADE)
    current = models.BooleanField(null=False, default=True)
    about = models.TextField(max_length='100', null=False)

    red_histogram = models.ImageField(upload_to='histograms/', null=True);
    green_histogram = models.ImageField(upload_to='histograms/', null=True);
    blue_histogram = models.ImageField(upload_to='histograms/', null=True);
    gray_histogram = models.ImageField(upload_to='histograms/', null=True);


class ImageToBin(models.Model):
    bin_low = models.TextField()
    bin_high = models.TextField()
    bin_limit = models.IntegerField()

    class Meta:
        managed = False

class ImageContrast(models.Model):
    contrast_value = models.IntegerField()
    bright_value = models.IntegerField()

    class Meta:
        managed = False


class ImageNoiseSaltPepper(models.Model):
    value = models.FloatField()

    class Meta:
        managed = False