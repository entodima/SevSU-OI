# SevSU-OI
СевГУ ИС 3 курс 2 семестр - Обработка изображений 
Лабораторные работы


Работает на django 2.1, python 3

Для запуска необходимо:
  source env/bin/activate
1. Произвести миграцию
  ./manage.py makemigrations
  ./manage.py migrate
2.Создать суперпользователя
  .manage.py createsuperuser
3. Запустить сервер
  ./manage.py runserver
