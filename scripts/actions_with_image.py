from PIL import Image, ImageDraw
from colors import hex
import numpy as np
import matplotlib.pyplot as plt
import random
import math


def get_image_format(path_img):
    image = Image.open('media/'+str(path_img))
    return image.format


def get_image_size(path_img):
    image = Image.open('media/'+str(path_img))
    return image.size


def image_to_gray(path_img):
    path = ('media/'+str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)  # Создаем инструмент для рисования
    width = image.size[0]  # Определяем ширину
    height = image.size[1]  # Определяем высоту
    pix = image.load()  # Выгружаем значения пикселей

    # в оттенки серого
    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][1]
            b = pix[i, j][2]
            s = int(r * 0.3) + int(g * 0.59) + int(b * 0.11)
            draw.point((i, j), (s, s, s))
    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_bin(path_img, bin_high, bin_low, bin_limit):
    path = ('media/'+str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    rgb_bin_high = str(hex(bin_high).rgb).split(', ')
    rgb_bin_low = str(hex(bin_low).rgb).split(', ')

    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][1]
            b = pix[i, j][2]
            s = int(r * 0.3) + int(g * 0.59) + int(b * 0.11)
            if s > bin_limit:
                sr = int(rgb_bin_high[0])
                sg = int(rgb_bin_high[1])
                sb  = int(rgb_bin_high[2])
            else:
                sr = int(rgb_bin_low[0])
                sg = int(rgb_bin_low[1])
                sb  = int(rgb_bin_low[2])
            draw.point((i, j), (sr,  sg,  sb))
    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_negative(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][1]
            b  = pix[i, j][2]

            draw.point((i, j), (abs(255-r), abs(255-g), abs(255-b) ))
    image.save(path)
    del draw
    return create_histogram(path_img)


def image_set_contrast(path_img, contrast_value, bright_value):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    r_avg = 0
    g_avg = 0
    b_avg = 0
    pixels_sum = 0

    for i in range(width):
        for j in range(height):
            r_avg += pix[i, j][0]
            g_avg += pix[i, j][1]
            b_avg += pix[i, j][2]
            pixels_sum += 1

    r_avg = int(r_avg / pixels_sum)
    g_avg = int(g_avg / pixels_sum)
    b_avg = int(b_avg / pixels_sum)

    if contrast_value != 0:
        for i in range(width):
            for j in range(height):
                r = (contrast_value * (pix[i, j][0] - r_avg) + r_avg) + bright_value  # NY = K*(Y-dY)+dY
                g = (contrast_value * (pix[i, j][1] - g_avg) + g_avg) + bright_value
                b  = (contrast_value * (pix[i, j][2] - b_avg) + b_avg) + bright_value

                draw.point((i, j), (r,  g,  b) )
    else:
        for i in range(width):
            for j in range(height):
                draw.point((i, j), (pix[i, j][0] + bright_value,
                                    pix[i, j][1] + bright_value,
                                    pix[i, j][2] + bright_value))

    print(contrast_value)
    image.save(path)
    del draw
    return create_histogram(path_img)


def create_histogram(path_img):
    path_img = str(path_img)

    def histogram(s, color):
        red_histogram_plt_path = path_img.replace('processed_images/', 'histograms/')
        red_histogram_plt_path = red_histogram_plt_path.split('.')
        red_histogram_plt_path = red_histogram_plt_path[0] + '-' + color + '.' + red_histogram_plt_path[-1]

        x = range(len(s))
        ax = plt.gca()
        ax.set_yticks([])
        ax.set_xticks([])
        ax.bar(x, s, color=color)

        plt.savefig('media/' + red_histogram_plt_path)
        plt.clf()
        return red_histogram_plt_path

    path = ('media/' + str(path_img))

    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    red_histogram = np.zeros(256, dtype=np.int)

    green_histogram = np.zeros(256, dtype=np.int)
    blue_histogram = np.zeros(256, dtype=np.int)
    gray_histogram = np.zeros(256, dtype=np.int)

    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][1]
            b  = pix[i, j][2]
            S = int(r * 0.3) + int(g * 0.59) + int(b  * 0.11)

            red_histogram[r] += 1
            green_histogram[g] += 1
            blue_histogram[b] += 1
            gray_histogram[S] += 1

    return [histogram(red_histogram, 'red'),
            histogram(green_histogram, 'green'),
            histogram(blue_histogram, 'blue'),
            histogram(gray_histogram, 'gray')]


def image_set_noise_salt_pepper(path_img, value):
    path = ('media/'+str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][1]
            b = pix[i, j][2]

            temp = random.random()
            if temp <= value:
                temp = random.random()
                if temp > 0.5:
                    r = 255
                    g = 255
                    b = 255
                else:
                    r = 0
                    g = 0
                    b = 0

            draw.point((i, j), (r,  g,  b))
    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_lin(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    k = 9  # нормирующий множитель

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i+1 == width or j+1 == height:
                pass
            else:
                r_summ = pix[i-1, j-1][0]
                g_summ = pix[i-1, j-1][1]
                b_summ = pix[i-1, j-1][2]

                r_summ = r_summ + pix[i-1, j][0]
                g_summ = g_summ + pix[i-1, j][1]
                b_summ = b_summ + pix[i-1, j][2]

                r_summ = r_summ + pix[i-1, j+1][0]
                g_summ = g_summ + pix[i-1, j+1][1]
                b_summ = b_summ + pix[i-1, j+1][2]

                r_summ = r_summ + pix[i, j-1][0]
                g_summ = g_summ + pix[i, j-1][1]
                b_summ = b_summ + pix[i, j-1][2]

                r_summ = r_summ + pix[i, j][0]
                g_summ = g_summ + pix[i, j][1]
                b_summ = b_summ + pix[i, j][2]

                r_summ = r_summ + pix[i, j+1][0]
                g_summ = g_summ + pix[i, j+1][1]
                b_summ = b_summ + pix[i, j+1][2]

                r_summ = r_summ + pix[i+1, j-1][0]
                g_summ = g_summ + pix[i+1, j-1][1]
                b_summ = b_summ + pix[i+1, j-1][2]

                r_summ = r_summ + pix[i+1, j][0]
                g_summ = g_summ + pix[i+1, j][1]
                b_summ = b_summ + pix[i+1, j][2]

                r_summ = r_summ + pix[i+1, j+1][0]
                g_summ = g_summ + pix[i+1, j+1][1]
                b_summ = b_summ + pix[i+1, j+1][2]


                r_summ = r_summ/k
                g_summ = g_summ/k
                b_summ = b_summ/k


                #r = pix[i, j][0]
                #g = pix[i, j][1]
                #b = pix[i, j][2]

                draw.point((i, j), (int(r_summ), int(g_summ), int(b_summ)))
    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_med(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i+1 == width or j+1 == height:
                pass
            else:
                r = [pix[i, j - 1][0], pix[i, j][0], pix[i, j + 1][0]]
                g = [pix[i, j - 1][1], pix[i, j][1], pix[i, j + 1][1]]
                b = [pix[i, j - 1][2], pix[i, j][2], pix[i, j + 1][2]]

                r.sort()
                g.sort()
                b.sort()

                draw.point((i, j), (r[1], g[1], b[1]))

    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_kir(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i + 1 == width or j + 1 == height:
                pass
            else:
                r = [pix[i - 1, j - 1][0], pix[i - 1, j][0], pix[i - 1, j + 1][0],
                    pix[i, j - 1][0], pix[i, j + 1][0],
                    pix[i + 1, j - 1][0], pix[i + 1, j][0], pix[i + 1, j + 1][0]]

                g = [pix[i - 1, j - 1][1], pix[i - 1, j][1], pix[i - 1, j + 1][1],
                    pix[i, j - 1][1], pix[i, j + 1][1],
                    pix[i + 1, j - 1][1], pix[i + 1, j][1], pix[i + 1, j + 1][1]]

                b = [pix[i - 1, j - 1][2], pix[i - 1, j][2], pix[i - 1, j + 1][2],
                    pix[i, j - 1][2], pix[i, j + 1][2],
                    pix[i + 1, j - 1][2], pix[i + 1, j][2], pix[i + 1, j + 1][2]]

                s_r = []
                s_g = []
                s_b = []

                for k in range(8):
                    s_r.append(r[k] + add_mod8(r[1], r[k]) + add_mod8(r[2], r[k]))
                    s_g.append(g[k] + add_mod8(g[1], g[k]) + add_mod8(g[2], g[k]))
                    s_b.append(b[k] + add_mod8(b[1], b[k]) + add_mod8(b[2], b[k]))

                t_r = []
                t_g = []
                t_b = []

                for k in range(8):
                    t_r.append(add_mod8(r[3], r[k]) + add_mod8(r[4], r[k]) + add_mod8(r[5], r[k]) + add_mod8(r[6], r[k]) + add_mod8(r[7], r[k]))
                    t_g.append(add_mod8(g[3], g[k]) + add_mod8(g[4], g[k]) + add_mod8(g[5], g[k]) + add_mod8(g[6], g[k]) + add_mod8(g[7], g[k]))
                    t_b.append(add_mod8(b[3], b[k]) + add_mod8(b[4], b[k]) + add_mod8(b[5], b[k]) + add_mod8(g[6], b[k]) + add_mod8(b[7], b[k]))

                f_r = []
                f_g = []
                f_b = []

                for k in range(8):
                    f_r.append(math.fabs(5*s_r[k] - 3*t_r[k]))
                    f_g.append(math.fabs(5*s_g[k] - 3*t_g[k]))
                    f_b.append(math.fabs(5*s_b[k] - 3*t_b[k]))

                f_r = max(f_r)
                f_g = max(f_g)
                f_b = max(f_b)

                draw.point((i, j), (int(f_r), int(f_g), int(f_b)))

    image.save(path)
    del draw
    return create_histogram(path_img)


def add_mod8(x, y):
    summ = x + y
    if summ > 7:
        summ = summ - 8
    return  summ


def image_to_lap(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    lapl = [-1, -2, -1, -2, 12, -2, -1, -2, -1]

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i + 1 == width or j + 1 == height:
                pass
            else:
                r = [pix[i - 1, j - 1][0] * lapl[0],
                     pix[i - 1, j][0] * lapl[1],
                     pix[i - 1, j + 1][0] * lapl[2],
                    pix[i, j - 1][0] * lapl[3],
                     pix[i, j][0] * lapl[4],
                     pix[i, j + 1][0] * lapl[5],
                    pix[i + 1, j - 1][0] * lapl[6],
                     pix[i + 1, j][0] * lapl[7],
                     pix[i + 1, j + 1][0] * lapl[8]]

                g = [pix[i - 1, j - 1][1] * lapl[0],
                     pix[i - 1, j][1] * lapl[1],
                     pix[i - 1, j + 1][1] * lapl[2],
                    pix[i, j - 1][1] * lapl[3],
                     pix[i, j][1] * lapl[4],
                     pix[i, j + 1][1] * lapl[5],
                    pix[i + 1, j - 1][1] * lapl[6],
                     pix[i + 1, j][1] * lapl[7],
                     pix[i + 1, j + 1][1] * lapl[8]]

                b = [pix[i - 1, j - 1][2] * lapl[0],
                     pix[i - 1, j][2] * lapl[1],
                     pix[i - 1, j + 1][2] * lapl[2],
                    pix[i, j - 1][2] * lapl[3],
                     pix[i, j][2] * lapl[4],
                     pix[i, j + 1][2] * lapl[5],
                    pix[i + 1, j - 1][2] * lapl[6],
                     pix[i + 1, j][2] * lapl[7],
                     pix[i + 1, j + 1][2] * lapl[8]]

                r = sum(r)
                g = sum(g)
                b = sum(b)

                draw.point((i, j), (int(r), int(g), int(b)))

    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_rob(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    lapl = [-1, -2, -1, -2, 12, -2, -1, -2, -1]

    for i in range(width):
        for j in range(height):
            if i + 1 == width or j + 1 == height:
                pass
            else:
                g_r = math.fabs(pix[i, j][0] - pix[i + 1, j + 1][0]) + math.fabs(pix[i, j + 1][0] - pix[i+1, j][0])
                g_g = math.fabs(pix[i, j][1] - pix[i + 1, j + 1][1]) + math.fabs(pix[i, j + 1][1] - pix[i+1, j][1])
                g_b = math.fabs(pix[i, j][2] - pix[i + 1, j + 1][2]) + math.fabs(pix[i, j + 1][2] - pix[i+1, j][2])

                draw.point((i, j), (int(g_r), int(g_g), int(g_b)))

    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_sob(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i + 1 == width or j + 1 == height:
                pass
            else:
                x_r = (pix[i-1, j-1][0] + 2*pix[i-1, j][0] + pix[i-1, j+1][0]) - (pix[i+1, j-1][0] + 2*pix[i+1, j][0] + pix[i+1, j+1][0])
                y_r = (pix[i-1, j-1][0] + 2*pix[i, j-1][0] + pix[i+1, j-1][0]) - (pix[i-1, j+1][0] + 2*pix[i, j+1][0] + pix[i+1, j+1][0])

                x_g = (pix[i - 1, j - 1][1] + 2 * pix[i - 1, j][1] + pix[i - 1, j + 1][1]) - (
                            pix[i + 1, j - 1][1] + 2 * pix[i + 1, j][1] + pix[i + 1, j + 1][1])
                y_g = (pix[i - 1, j - 1][1] + 2 * pix[i, j - 1][1] + pix[i + 1, j - 1][1]) - (
                            pix[i - 1, j + 1][1] + 2 * pix[i, j + 1][1] + pix[i + 1, j + 1][1])

                x_b = (pix[i - 1, j - 1][2] + 2 * pix[i - 1, j][2] + pix[i - 1, j + 1][2]) - (
                            pix[i + 1, j - 1][2] + 2 * pix[i + 1, j][2] + pix[i + 1, j + 1][2])
                y_b = (pix[i - 1, j - 1][2] + 2 * pix[i, j - 1][2] + pix[i + 1, j - 1][2]) - (
                            pix[i - 1, j + 1][2] + 2 * pix[i, j + 1][2] + pix[i + 1, j + 1][2])

                g_r = math.sqrt(x_r*x_r + y_r*y_r)
                g_g = math.sqrt(x_g*x_g + y_g*y_g)
                g_b = math.sqrt(x_b*x_b + y_b*y_b)

                draw.point((i, j), (int(g_r), int(g_g), int(g_b)))

    image.save(path)
    del draw
    return create_histogram(path_img)


def image_to_uol(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i + 1 == width or j + 1 == height:
                pass
            else:

                f_r = math.log(( (pix[i, j][0]+1)/(pix[i - 1, j][0]+1) *
                                 (pix[i, j][0]+1)/(pix[i, j + 1][0]+1) *
                                 (pix[i, j][0]+1)/(pix[i + 1, j][0]+1) *
                                 (pix[i, j][0]+1)/(pix[i, j-1][0]+1))
                               ) / 4
                f_g = math.log(((pix[i, j][1] + 1) / (pix[i - 1, j][1] + 1) *
                                (pix[i, j][1] + 1) / (pix[i, j + 1][1] + 1) *
                                (pix[i, j][1] + 1) / (pix[i + 1, j][1] + 1) *
                                (pix[i, j][1] + 1) / (pix[i, j - 1][1] + 1))
                               ) / 4
                f_b = math.log(((pix[i, j][2] + 1) / (pix[i - 1, j][2] + 1) *
                                (pix[i, j][2] + 1) / (pix[i, j + 1][2] + 1) *
                                (pix[i, j][2] + 1) / (pix[i + 1, j][2] + 1) *
                                (pix[i, j][2] + 1) / (pix[i, j - 1][2] + 1))
                               ) / 4

                draw.point((i, j), (int(f_r*1000), int(f_g*1000), int(f_b*1000)))

    image.save(path)
    del draw
    return create_histogram(path_img)

def image_to_sta(path_img):
    path = ('media/' + str(path_img))
    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()


    for i in range(width):
        for j in range(height):
            if i == 0 or j == 0 or i + 1 == width or j + 1 == height:
                pass
            else:
                summ1_r = pix[i-1, j][0] + pix[i, j][0] + pix[i+1, j][0] + pix[i-1, j-1][0] + pix[i, j-1][0] + pix[i+1, j-1][0] + pix[i-1, j+1][0] + pix[i, j+1][0] + pix[i+1, j+1][0]
                summ1_g = pix[i-1, j][1] + pix[i, j][1] + pix[i+1, j][1] + pix[i-1, j-1][1] + pix[i, j-1][1] + pix[i+1, j-1][1] + pix[i-1, j+1][1] + pix[i, j+1][1] + pix[i+1, j+1][1]
                summ1_b = pix[i-1, j][2] + pix[i, j][2] + pix[i+1, j][2] + pix[i-1, j-1][2] + pix[i, j-1][2] + pix[i+1, j-1][2] + pix[i-1, j+1][2] + pix[i, j+1][2] + pix[i+1, j+1][2]

                mu_r = 1 / 9 * summ1_r
                mu_g = 1 / 9 * summ1_g
                mu_b = 1 / 9 * summ1_b

                summ2_r = (pix[i-1, j][0]-mu_r)**2 + (pix[i, j][0]-mu_r)**2 + (pix[i+1, j][0]-mu_r)**2 + pix[i-1, j-1][0] + (pix[i, j-1][0]-mu_r)**2 + (pix[i+1, j-1][0]-mu_r)**2 + (pix[i-1, j+1][0]-mu_r)**2 + (pix[i, j+1][0]-mu_r)**2 + (pix[i+1, j+1][0]-mu_r)**2
                summ2_g = (pix[i-1, j][1]-mu_g)**2 + (pix[i, j][1]-mu_g)**2 + (pix[i+1, j][1]-mu_g)**2 + pix[i-1, j-1][1] + (pix[i, j-1][1]-mu_g)**2 + (pix[i+1, j-1][1]-mu_g)**2 + (pix[i-1, j+1][1]-mu_g)**2 + (pix[i, j+1][1]-mu_g)**2 + (pix[i+1, j+1][1]-mu_g)**2
                summ2_b = (pix[i-1, j][2]-mu_b)**2 + (pix[i, j][2]-mu_b)**2 + (pix[i+1, j][2]-mu_b)**2 + pix[i-1, j-1][2] + (pix[i, j-1][2]-mu_b)**2 + (pix[i+1, j-1][2]-mu_b)**2 + (pix[i-1, j+1][2]-mu_b)**2 + (pix[i, j+1][2]-mu_b)**2 + (pix[i+1, j+1][2]-mu_b)**2

                tau_r = math.sqrt(1 / 9 * summ2_r)
                tau_g = math.sqrt(1 / 9 * summ2_g)
                tau_b = math.sqrt(1 / 9 * summ2_b)

                val = 0

                draw.point((i, j-1), (int(tau_r * pix[i, j-1][0] + val), int(tau_g * pix[i, j-1][1] + val), int(tau_b * pix[i, j-1][2] + val)))
                draw.point((i, j),   (int(tau_r * pix[i, j][0] + val), int(tau_g * pix[i, j][1] + val), int(tau_b * pix[i, j][2] + val)))
                draw.point((i, j+1), (int(tau_r * pix[i, j+1][0] + val), int(tau_g * pix[i, j+1][1] + val), int(tau_b * pix[i, j+1][2] + val)))

                draw.point((i-1, j-1), (int(tau_r * pix[i-1, j-1][0] + val), int(tau_g * pix[i-1, j-1][1] + val), int(tau_b * pix[i-1, j-1][2] + val)))
                draw.point((i-1, j),   (int(tau_r * pix[i-1, j][0] + val), int(tau_g * pix[i-1, j][1] + val), int(tau_b * pix[i-1, j][2] + val)))
                draw.point((i-1, j+1), (int(tau_r * pix[i-1, j+1][0] + val), int(tau_g * pix[i-1, j+1][1] + val), int(tau_b * pix[i-1, j+1][2] + val)))

                draw.point((i+1, j-1), (int(tau_r * pix[i+1, j-1][0] + val), int(tau_g * pix[i+1, j-1][1] + val), int(tau_b * pix[i+1, j-1][2] + val)))
                draw.point((i+1, j), (int(tau_r * pix[i+1, j][0] + val), int(tau_g * pix[i+1, j][1] + val), int(tau_b * pix[i+1, j][2] + val)))
                draw.point((i+1, j+1), (int(tau_r * pix[i+1, j+1][0] + val), int(tau_g * pix[i+1, j+1][1] + val), int(tau_b * pix[i+1, j+1][2] + val)))

    image.save(path)
    del draw
    return create_histogram(path_img)